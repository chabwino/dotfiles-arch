local status_ok, wk = pcall(require, "which-key")
if not status_ok then
  return
end

wk.setup {
    plugins = {
    marks = true, -- shows a list of your marks on ' and `
    registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
    spelling = {
      enabled = false, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
      suggestions = 20, -- how many suggestions should be shown in the list?
    },
    -- the presets plugin, adds help for a bunch of default keybindings in Neovim
    -- No actual key bindings are created
    presets = {
      operators = true, -- adds help for operators like d, y, ... and registers them for motion / text object completion
      motions = true, -- adds help for motions
      text_objects = true, -- help for text objects triggered after entering an operator
      windows = true, -- default bindings on <c-w>
      nav = true, -- misc bindings to work with windows
      z = true, -- bindings for folds, spelling and others prefixed with z
      g = true, -- bindings for prefixed with g
    },
  },
  -- add operators that will trigger motion and text object completion
  -- to enable all native operators, set the preset / operators plugin above
  operators = { gc = "Comments" },
  key_labels = {
    -- override the label used to display some keys. It doesn't effect WK in any other way.
    -- For example:
    -- ["<space>"] = "SPC",
    -- ["<cr>"] = "RET",
    -- ["<tab>"] = "TAB",
  },
  icons = {
    breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
    separator = "➜", -- symbol used between a key and it's label
    group = "+", -- symbol prepended to a group
  },
  popup_mappings = {
    scroll_down = '<c-d>', -- binding to scroll down inside the popup
    scroll_up = '<c-u>', -- binding to scroll up inside the popup
  },
  window = {
    border = "none", -- none, single, double, shadow
    position = "top", -- bottom, top
    margin = { 1, 0, 1, 0 }, -- extra window margin [top, right, bottom, left]
    padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
    winblend = 0
  },
  layout = {
    height = { min = 4, max = 25 }, -- min and max height of the columns
    width = { min = 20, max = 50 }, -- min and max width of the columns
    spacing = 3, -- spacing between columns
    align = "left", -- align columns left, center or right
  },
  ignore_missing = false, -- enable this to hide mappings for which you didn't specify a label
  hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "call", "lua", "^:", "^ "}, -- hide mapping boilerplate
  show_help = true, -- show help message on the command line when the popup is visible
  triggers = "auto", -- automatically setup triggers
  -- triggers = {"<leader>"} -- or specify a list manually
  triggers_blacklist = {
    -- list of mode / prefixes that should never be hooked by WhichKey
    -- this is mostly relevant for key maps that start with a native binding
    -- most people should not need to change this
    i = { "j", "k" },
    v = { "j", "k" },
  },
}

wk.register({
    c = { "<cmd>Bdelete<cr>", "close bufferline" },
    e = { "<cmd>NvimTreeToggle<cr>", "Toggle NvimTree" },
    f = {
        name = "find",
        f = { "<cmd>Telescope find_files<cr>", "Find File" },
        r = { "<cmd>Telescope oldfiles<cr>", "Open Recent File" },
        g = { "<cmd>Telescope live_grep<cr>", "Find in files" },
    },
    g = {
        name = "git",
        h = { "<cmd>Gitsigns prev_hunk<cr>", "previous hunk" },
        p = { "<cmd>Gitsigns preview_hunk<CR>", "preview hunk" },
        l = { "<cmd>Gitsigns next_hunk<CR>", "next hunk" },
    },
    l = {
        name = "Lsp",
        D = { "<cmd>lua vim.lsp.buf.declaration()<CR>", "declaration" },
        d = { "<cmd>lua vim.lsp.buf.definition()<CR>", "definition" }, 
        f = { "<cmd>lua vim.lsp.buf.formatting_sync()<cr>", "format file" },
        h = { "<cmd>lua vim.lsp.buf.hover()<CR>" , "hover" },
        i = { "<cmd>lua vim.lsp.buf.implementation()<CR>", "implementation" },
        l = { "<cmd>lua vim.diagnostic.open_float({ border = 'rounded' })<CR>", "show error text" },
        r = { "<cmd>lua vim.lsp.buf.references()<CR>", "references" },
        s = { "<cmd>LspInfo<cr>", "server info" },
    },
    p = {
        name = "packer",
        s = { "<cmd>PackerStatus<cr>", "status" },
        u = { "<cmd>PackerUpdate<cr>", "update" },
        y = { "<cmd>PackerSync<cr>", "sync" },
    },
    r = { "grr", "smart rename" },
    t = {
        name = "trouble",
        d = { "<cmd>TroubleToggle document_diagnostics<cr>", "document diagnostics" },
        q = { "<cmd>TroubleToggle quickfix<cr>", "quick fix" },
        r = { "<cmd>TroubleToggle lsp_references<cr>", "references" },
        t = { "<cmd>TroubleToggle<cr>", "toggle diagnostics window" },
        w = { "<cmd>TroubleToggle workspace_diagnostics<cr>", "workspace diagnostics" },
    },
    w = {
        name = "window",
        l = { "<C-W>L", "move to far right" },
        h = { "<C-W>H", "move to far left" },
    }
}, { prefix = "<leader>" })
