#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

#run #!/bin/bash

run picom -b --config  $HOME/.config/awesome/picom.conf
run /usr/bin/emacs -daemon &
run autorandr -c
run udiskie &
run xscreensaver -no-splash
run blueman-applet
run blueman-tray
run xfce4-clipman
run pcloud
run kdeconnect-indicator
