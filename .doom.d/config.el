;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
(setq auth-sources '("~/.authinfo.gpg" "~/.authinfo" "~/.netrc"))

(add-to-list 'load-path "~/.doom.d/mu4e")
(require 'mu4e)

(use-package mu4e
  :ensure nil
  ;;:defer 20
  :config

  (setq mu4e-change-filenames-when-moving t)

  (setq mu4e-update-interval (* 2 60))
  (setq mu4e-get-mail-command "mbsync -c ~/.doom.d/mu4e/.mbsyncrc -a")
  (setq mu4e-maildir "~/Maildir")

  ;; Don't ask how to send email, just use emacs internal method
  (setq message-send-mail-function 'smtpmail-send-it)

  ;; When composing an email, ask which context to use.
  ;; Alternative could be ask-if-none, this would only ask if no context is selected yet.
  (setq mu4e-compose-context-policy 'ask)

  (setq mu4e-compose-dont-reply-to-self t)
  (setq mu4e-compose-format-flowed t)
  (setq mu4e-compose-complete-addresses t)
  (setq mu4e-compose-complete-only-personal nil)
  (setq mu4e-headers-fields
        '((:human-date . 12)
         (:flags . 7)
         (:from . 25)
         (:subject)))

  (setq mu4e-contexts
        (list
         ;;gmx
         (make-mu4e-context
          :name "gmx"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/gmx" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "dominikseeger@gmx.net")
                  (user-full-name . "Dominik Seeger")
                  (smtpmail-smtp-server . "mail.gmx.net")
                  (smtpmail-smtp-service . 465)
                  (smtpmail-stream-type . ssl)
                  (mu4e-drafts-folder . "/gmx/Entw\&APw-rfe")
                  (mu4e-sent-folder . "/gmx/Gesendet")
                  (mu4e-refile-folder . "/gmx/All Mail")
                  (mu4e-trash-folder . "/gmx/Gel\&APY-scht")))

         ;;psi-main
         (make-mu4e-context
          :name "app-psi"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/app-psi" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "dominikseeger@psicheck.app")
                  (user-full-name . "Dominik Seeger")
                  (smtpmail-smtp-server . "smtppro.zoho.eu")
                  (smtpmail-smtp-service . 465)
                  (smtpmail-stream-type . ssl)
                  (mu4e-drafts-folder . "/app-psi/Drafts")
                  (mu4e-sent-folder . "/app-psi/Sent")
                  (mu4e-refile-folder . "/app-psi/All Mail")
                  (mu4e-trash-folder . "/app-psi/Trash")))

        ;;psi-ch
         (make-mu4e-context
          :name "ch-psi"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/ch-psi" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "dominikseeger@psicheck.ch")
                  (user-full-name . "Dominik Seeger")
                  (smtpmail-smtp-server . "smtppro.zoho.eu")
                  (smtpmail-smtp-service . 465)
                  (smtpmail-stream-type . ssl)
                  (mu4e-drafts-folder . "/ch-psi/Drafts")
                  (mu4e-sent-folder . "/ch-psi/Sent")
                  (mu4e-refile-folder . "/ch-psi/All Mail")
                  (mu4e-trash-folder . "/ch-psi/Trash")))


        ;;outlook
         (make-mu4e-context
          :name "outlook"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/outlook" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "chabwino13@outlook.com")
                  (user-full-name . "Dominik Seeger")
                  (smtpmail-smtp-server . "smtp-mail.outlook.com")
                  (smtpmail-smtp-service . 587)
                  (smtpmail-stream-type . nil)
                  (mu4e-drafts-folder . "/outlook/Drafts")
                  (mu4e-sent-folder . "/outlook/Sent")
                  (mu4e-refile-folder . "/outlook/All Mail")
                  (mu4e-trash-folder . "/outlook/Deleted")))))



  (setq mu4e-maildir-shortcuts
        '(("/gmx/INBOX" . ?g)
          ("/psi-main/INBOX" . ?p)
          ("/psi-ch/INBOX" . ?c)))

  (add-to-list 'mu4e-bookmarks '("m:/gmx/INBOX or m:/app-psi/INBOX or m:/app-psi/INBOX/Support or m:/ch-psi/INBOX or m:/ch-psi/INBOX/Server or m:/outlook/INBOX" "All Inboxes" ?i)))

;;(mu4e t)
(setq x-select-enable-clipboard-manager nil)

(defun efs/lookup-password (&rest keys)
  (let ((result (apply #'auth-source-search keys)))
    (if result
        (funcall (plist-get (car result) :secret))
      nil)))

(use-package org-mime
  :ensure t
  :config
  (setq org-mime-export-options '(:section-numbers nil
                                  :with-author nil
                                  :with-toc nil))
  (add-hook 'message-send-hook 'org-mime-htmlize))
