#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# launch startx after login
[[ $(fgconsole 2>/dev/null) == 1 ]] && exec startx -- vt1
