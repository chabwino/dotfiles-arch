#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

### EXPORT ###
export EDITOR='vim'
export VISUAL='notepadqq'
export HISTCONTROL=ignoreboth:erasedups
export PAGER='less'


#PS1="\[\033[00;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ "
PS1="\[\033[00;32m\]> \[$(tput sgr0)\]\[$(tput sgr0)\]\[\033[01;34m\]\W\[$(tput sgr0)\]\\$\[$(tput sgr0)\] "

PATH="$HOME/.bin:$PATH"
PATH="$HOME/.emacs.d/bin:$PATH"
PATH="$HOME/.local/bin:$PATH"
PATH="$HOME/Applications:$PATH"

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

### ALIASES ###
alias ..='cd ..'
alias emacs="emacsclient -c -a 'emacs'"
alias psicheck='cd ~/Projects/Personal/sd-ddb'
#work
alias dco='docker-compose'
alias dcu='docker-compose up -d'
alias dcd='docker-compose down'
alias dce='docker-compose exec'
alias webs='cd ~/Projects/hosttech/WSC'
alias websc='cd ~/Projects/hosttech/WSC/websitecreator-client'
alias websa='cd ~/Projects/hosttech/WSC/websitecreator-api'
alias websw='cd ~/Projects/hosttech/WSC/websitecreator'
alias myhtech='cd ~/Projects/hosttech/MyHosttech/myhosttech'
#sail
alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'
alias sailu='[ -f sail ] && bash sail || bash vendor/bin/sail up -d'
alias saild='[ -f sail ] && bash sail || bash vendor/bin/sail down'
alias sailart='[ -f sail ] && bash sail || bash vendor/bin/sail artisan'
alias sailmfs='[ -f sail ] && bash sail || bash vendor/bin/sail artisan migrate:fresh --seed'
alias sailtinker='[ -f sail ] && bash sail || bash vendor/bin/sail artisan tinker'
#list
#alias ls='ls --color=auto'
#alias la='ls -a'
#alias ll='ls -alFh'
alias ls='lsd'
alias ls='lsd -a'
alias ll='lsd -lA --group-dirs first'
alias l='ls'
alias l.="ls -A | egrep '^\.'"
#fix obvious typo's
alias cd..='cd ..'
alias pdw='pwd'
alias udpate='sudo pacman -Syyu'
alias upate='sudo pacman -Syyu'
alias updte='sudo pacman -Syyu'
alias updqte='sudo pacman -Syyu'
alias upqll='paru -Syu --noconfirm'
alias upal='paru -Syu --noconfirm'
## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
#readable output
alias df='df -h'
# Aliases for software managment
# pacman or pm
alias pacman='sudo pacman --color auto'
alias update='sudo pacman -Syyu'
# yay
alias yay='yay --color auto'
#grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
#Cleanup orphaned packages
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'
#shutdown or reboot
alias sdn="sudo shutdown now"
alias sr="sudo reboot"
# # ex = EXtractor for all kinds of archives
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   tar xf $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

pfetch
